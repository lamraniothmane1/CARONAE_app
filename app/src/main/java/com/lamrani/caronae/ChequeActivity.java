package com.lamrani.caronae;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.lamrani.caronae.API.controllers.ScanController;
import com.lamrani.caronae.API.models.CINResponse;
import com.lamrani.caronae.helper.RoundedTransformation;
import com.scanlibrary.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequeActivity extends AppCompatActivity {

    private TextView tv_cmc7;
    private ImageView iv_cheque;
    private Uri uri_result;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        if(intent.getExtras() != null){
            uri_result = Uri.parse(intent.getExtras().getString("uri"));
        }

        init();


        if(uri_result != null){
            call_scan_cin(uri_result);
        }
        else {
            showScanError();
        }

    }

    /**
     *
     * set CMC7 line result
     */
    public void setCMC7(String smc7_string){

        if(smc7_string != null){
            tv_cmc7.setText(smc7_string);
        }
        else{
            tv_cmc7.setText(R.string.unavailable);
        }

    }

    /*private void updateView(String cmc7_string) {
        if(cmc7_string != null){
            tv_cin.setText(cinResponse.getCIN());
            tv_first_name.setText(cinResponse.getFirstName());
            tv_last_name.setText(cinResponse.getSecondName());
            tv_birth_day.setText(cinResponse.getBirthDay());
            tv_birth_place.setText(cinResponse.getBrithPlace());
            tv_expiry_date.setText(cinResponse.getExpiryDate());
            Picasso.with(getApplicationContext())
                    .load(OperatingApiClient.IP_SERVER + "/CaronaeUtilities" + cinResponse.getPhotoLink())
                    .into(iv_cin);
        }
        else{
            Toast.makeText(this, "Résultat nul", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void init() {

        iv_cheque = (ImageView) findViewById(R.id.iv_cheque);
        tv_cmc7 = (TextView) findViewById(R.id.tv_cmc7);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));

        iv_cheque.setImageURI(uri_result);
        Picasso.with(this)
                .load(uri_result)
                .transform(new RoundedTransformation(50, 0))
                .into(iv_cheque);
    }


    /**
     * Scan CIN call API
     *
     */
    public void call_scan_cin(Uri uri){
        try {

            progressDialog.show();

            Bitmap bitmapResult = com.scanlibrary.Utils.getBitmap(this, uri);
            //Log.i("Base 64 cropped Image", com.scanlibrary.Utils.encodeTobase64(bitmapResult));

            String base64Image_string = com.scanlibrary.Utils.encodeTobase64(bitmapResult);

            //generateNoteOnSD(getApplicationContext(), "caronae", Utils.encodeTobase64(bitmapResult));

            ScanController scanController = new ScanController();

            Call<String> call_scan = scanController.getCM7Result( base64Image_string);

            call_scan.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    Log.i("Retrofit", "Success !!");

                    if(response.body() != null){
                        String result_CIN = response.body();

                        Log.i("SCAN CMC7  works !", "CMC7: " + response.body());

                        setCMC7(result_CIN);

                    }
                    else{
                        Toast.makeText(ChequeActivity.this, R.string.scan_error, Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.i("Retrofit Call API", "Failure");
                    if(t.getMessage() != null){
                        Log.i("Caused by", t.getMessage());
                    }

                    showScanError();

                    //fragment.setCMC7(result_CMC7);
                }
            });



        } catch (IOException e) {
            e.printStackTrace();
            showScanError();
        }
    }

    public void showScanError(){
        progressDialog.dismiss();
        Toast.makeText(this, R.string.scan_error, Toast.LENGTH_SHORT).show();
    }
}
