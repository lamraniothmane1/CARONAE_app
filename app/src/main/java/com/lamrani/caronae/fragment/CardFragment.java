package com.lamrani.caronae.fragment;

/**
 * Created by Lamrani on 18/12/2017.
 */

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lamrani.caronae.MainActivity;
import com.lamrani.caronae.R;
import com.lamrani.caronae.helper.RoundedTransformation;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.Utils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

import static com.lamrani.caronae.helper.Utils.REQUEST_CHEQUE;
import static com.lamrani.caronae.helper.Utils.REQUEST_CIN;
import static com.scanlibrary.Utils.REQUEST_CODE;


public class CardFragment extends Fragment {

    private Button btn_action;
    private CardView cardView;

    public static Fragment getInstance(int position) {
        CardFragment f = new CardFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int position = getArguments().getInt("position");
        int id_view = R.layout.dashboard_carousel_card_cin;
        if(position == 1){
            id_view = R.layout.dashboard_carousel_card_cheque;
        }

        View view = inflater.inflate(id_view, container, false);




        /*TextView title = (TextView) view.findViewById(R.id.title);

        title.setText(String.format("Card %d", getArguments().getInt("position")));*/

        btn_action = (Button) view.findViewById(R.id.btn_action_card);

        switch (position){
            case 0: {
                // rounded cards
                ImageView iv_cin = view.findViewById(R.id.iv_cin);

                if(iv_cin != null){
                    Picasso.with(getContext())
                            .load(R.drawable.cin_img)
                            .transform(new RoundedTransformation(40, 2))
                            .into(iv_cin);
                }


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), ScanActivity.class);
                        MainActivity.request_scan_type = REQUEST_CIN;
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    }
                });

                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), ScanActivity.class);
                        MainActivity.request_scan_type = REQUEST_CIN;
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    }
                });

                break;
            }
            case 1: {

                // rounded images
                ImageView iv_cheque = view.findViewById(R.id.iv_cheque);
                if(iv_cheque != null){
                    Picasso.with(getContext())
                            .load(R.drawable.cheque_img)
                            .transform(new RoundedTransformation(40, 2))
                            .into(iv_cheque);

                }

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getContext(), "En cours de développement ...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ScanActivity.class);
                        //intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
                        MainActivity.request_scan_type = REQUEST_CHEQUE;
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    }
                });

                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getContext(), "En cours de développement ...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ScanActivity.class);
                        //intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
                        MainActivity.request_scan_type = REQUEST_CHEQUE;
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    }
                });



                break;
            }
        }



        return view;
    }

    // get byte Array Output Stream
    public byte[] getBaos(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public CardView getCardView() {
        return cardView;
    }


}