package com.lamrani.caronae.API.callbacks;

import com.lamrani.caronae.API.models.CINResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;


/**
 * Created by Lamrani on 21/09/2017.
 */

public interface ScanAPI {

    @Headers("Content-Type: application/json")
    @POST( "testt/api/reader")
    Call<String> getCM7Result(@HeaderMap Map<String, String> headers, @Body String base64Image_string);


    @Headers("Content-Type: application/json")
    @POST("CaronaeUtilities/api/cin")
    Call<CINResponse> getCINResponse(@HeaderMap Map<String, String> headers, @Body() String base64);

}
