package com.lamrani.caronae.API.controllers;



import com.lamrani.caronae.API.OperatingApiClient;
import com.lamrani.caronae.API.callbacks.ScanAPI;
import com.lamrani.caronae.API.models.CINResponse;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by Lamrani on 10/01/2018.
 */

public class ScanController {

    private ScanAPI scanAPI;

    public ScanController(){
        Retrofit retrofit = OperatingApiClient.getClient();
        scanAPI =  retrofit.create(ScanAPI.class);
    }

    /**
     * Get CM7 line from base64 Image
     */
    public Call<String> getCM7Result(String base64Image_string){
        return scanAPI.getCM7Result(OperatingApiClient.getHeader(), base64Image_string);
    }

    /**
     * Get CIN informations from base64 Image
     */
    public Call<CINResponse> getCINResult(String base64Image_string){
        return scanAPI.getCINResponse(OperatingApiClient.getHeader(), base64Image_string);
    }

}
