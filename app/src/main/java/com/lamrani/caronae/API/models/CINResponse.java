package com.lamrani.caronae.API.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lamrani on 18/01/2018.
 */

public class CINResponse implements Serializable{

    @SerializedName("FirstName")
    @Expose
    private String FirstName;

    @SerializedName("SecondName")
    @Expose
    private String SecondName;

    @SerializedName("BirthDay")
    @Expose
    private String BirthDay;

    @SerializedName("BrithPlace")
    @Expose
    private String BrithPlace;

    @SerializedName("ExpiryDate")
    @Expose
    private String ExpiryDate;

    @SerializedName("CIN")
    @Expose
    private String CIN;

    @SerializedName("photoLink")
    @Expose
    private String photoLink;

    public String getFirstName() {
        return FirstName.replace("\n", "");
    }

    public String getSecondName() {
        return SecondName.replace("\n", "");
    }

    public String getBirthDay() {
        return BirthDay.replace("\n", "");
    }

    public String getBrithPlace() {
        return BrithPlace.replace("\n", "");
    }

    public String getExpiryDate() {
        return ExpiryDate.replace("\n", "");
    }

    public String getCIN() {
        return CIN.replace("\n", "");
    }

    public String getPhotoLink() {
        return photoLink;
    }
}
