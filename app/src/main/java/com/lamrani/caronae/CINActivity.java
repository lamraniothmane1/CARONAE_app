package com.lamrani.caronae;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lamrani.caronae.API.OperatingApiClient;
import com.lamrani.caronae.API.controllers.ScanController;
import com.lamrani.caronae.API.models.CINResponse;
import com.lamrani.caronae.helper.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CINActivity extends AppCompatActivity {

    private TextView tv_first_name, tv_last_name, tv_cin, tv_expiry_date, tv_birth_day, tv_birth_place;
    private ImageView iv_cin;
    private Uri uri_result;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cin);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        init();

        Intent intent = getIntent();
        if(intent.getExtras() != null){
            uri_result = Uri.parse(intent.getExtras().getString("uri"));
        }

        if(uri_result != null){
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   call_scan_cin(uri_result);
               }
           });
        }
        else {
            showScanError();
        }

    }

    private void updateForm(CINResponse cinResponse) {
        if(cinResponse != null){
            tv_cin.setText(cinResponse.getCIN());
            tv_first_name.setText(cinResponse.getFirstName());
            tv_last_name.setText(cinResponse.getSecondName());
            tv_birth_day.setText(cinResponse.getBirthDay());
            tv_birth_place.setText(cinResponse.getBrithPlace());
            tv_expiry_date.setText(cinResponse.getExpiryDate());
            Picasso.with(getApplicationContext())
                    .load(OperatingApiClient.IP_SERVER + "/CaronaeUtilities" + cinResponse.getPhotoLink())
                    .into(iv_cin);
        }
        else{
            Toast.makeText(this, "Résultat nul", Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        tv_cin = (TextView) findViewById(R.id.tv_cin);
        tv_first_name = (TextView) findViewById(R.id.tv_first_name);
        tv_last_name = (TextView) findViewById(R.id.tv_last_name);
        tv_birth_day = (TextView) findViewById(R.id.tv_birth_date);
        tv_birth_place = (TextView) findViewById(R.id.tv_birth_place);
        tv_expiry_date = (TextView) findViewById(R.id.tv_expiry_date);
        iv_cin = (ImageView) findViewById(R.id.iv_cin);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
    }

    /**
     * Scan CIN call API
     *
     */
    public void call_scan_cin(Uri uri){
        try {

            progressDialog.show();

            Bitmap bitmapResult = com.scanlibrary.Utils.getBitmap(this, uri);
            //Log.i("Base 64 cropped Image", com.scanlibrary.Utils.encodeTobase64(bitmapResult));

            String base64Image_string = com.scanlibrary.Utils.encodeTobase64(bitmapResult);

            //generateNoteOnSD(getApplicationContext(), "caronae", Utils.encodeTobase64(bitmapResult));

            ScanController scanController = new ScanController();

            Call<CINResponse> call_scan = scanController.getCINResult( base64Image_string);

            if(Utils.isNetworkAvailable(this)){
                call_scan.enqueue(new Callback<CINResponse>() {
                    @Override
                    public void onResponse(Call<CINResponse> call, Response<CINResponse> response) {

                        Log.i("Retrofit", "Success !!");

                        if(response.body() != null){
                            CINResponse result_CIN = response.body();

                            Log.i("SCAN CIN result works !", "CIN: " + response.body().getCIN());

                            updateForm(result_CIN);

                        }
                        else{
                            Toast.makeText(CINActivity.this, R.string.scan_error, Toast.LENGTH_SHORT).show();
                        }

                        progressDialog.dismiss();

                    }

                    @Override
                    public void onFailure(Call<CINResponse> call, Throwable t) {
                        Log.i("Retrofit Call API", "Failure");
                        if(t.getMessage() != null){
                            Log.i("Caused by", t.getMessage());
                        }

                        showScanError();

                        //fragment.setCMC7(result_CMC7);
                    }
                });
            }
            else{
                Toast.makeText(this, "Veuillez vérifier votre connection internet !!", Toast.LENGTH_LONG).show();
            }



        } catch (IOException e) {
            e.printStackTrace();
            showScanError();
        }
    }

    public void showScanError(){
        progressDialog.dismiss();
        Toast.makeText(this, R.string.scan_error, Toast.LENGTH_LONG).show();
    }
}
