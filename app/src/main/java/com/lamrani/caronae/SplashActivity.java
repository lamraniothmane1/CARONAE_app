package com.lamrani.caronae;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashActivity extends Activity {

    private ImageView iv_logo_cih;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Hide action bar
        if(getActionBar() != null){
            getActionBar().hide();
        }

        // Image view (Logo cih)
        iv_logo_cih = (ImageView) findViewById(R.id.iv_logo_cih);

        // animate transition
        animate_ImageView();

        // Move to the home or Login activity
        start_next_activity();

    }

    /**
     *  move to the next activity
     */
    private void start_next_activity() {
        // this thread will sleep 2 seconds before executing
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    // pass to the Login activity
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        timer.start();
    }

    /**
     * Animate the logo to fade in when open activity
     */
    private void animate_ImageView() {
        Animation myAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.splash_transition);
        if(iv_logo_cih != null){
            iv_logo_cih.startAnimation(myAnimation);
        }
    }
}
