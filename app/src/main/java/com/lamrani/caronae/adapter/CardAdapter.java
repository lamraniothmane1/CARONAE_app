package com.lamrani.caronae.adapter;

/**
 * Created by Lamrani on 18/12/2017.
 */

import android.support.v7.widget.CardView;

// Interface adapter

public interface CardAdapter {

    public final int MAX_ELEVATION_FACTOR = 6;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}