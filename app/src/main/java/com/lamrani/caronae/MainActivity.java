package com.lamrani.caronae;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lamrani.caronae.adapter.CardFragmentPagerAdapter;
import com.lamrani.caronae.helper.RoundedTransformation;
import com.lamrani.caronae.helper.ShadowTransformer;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;
import com.scanlibrary.Utils;
import com.lamrani.caronae.API.controllers.ScanController;
import com.lamrani.caronae.API.models.CINResponse;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.lamrani.caronae.helper.Utils.REQUEST_CHEQUE;
import static com.lamrani.caronae.helper.Utils.REQUEST_CIN;
import static com.scanlibrary.Utils.REQUEST_CODE;


public class MainActivity extends AppCompatActivity {


    //private Button btn_scan_CIN;
    //private Button btn_scan_cheque;
    public CardFragmentPagerAdapter pagerAdapter;
    public ViewPager viewPager;
    public static int request_scan_type;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();

        configureCarouselCards();

    }

    private void init() {
        /*btn_scan_cheque = (Button) findViewById(R.id.btn_scan_cheque);
        btn_scan_cheque.setOnClickListener(new ScanChequeListener());
        btn_scan_CIN = (Button) findViewById(R.id.btn_scan_cin);
        btn_scan_CIN.setOnClickListener(new ScanCINClickListener());*/

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));



    }

   /* *//**
     * Scan cheque button onclick listener
     *//*
    private class ScanChequeListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

        }
    }

    *//**
     * Scan CIN button onclick listener
     *//*
    private class ScanCINClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            progressDialog.show();
            Intent intent = new Intent(MainActivity.this, ScanActivity.class);
            //intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            request_scan_type = REQUEST_CIN;
            startActivityForResult(intent, REQUEST_CODE);
            progressDialog.dismiss();
        }
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            // call Garbage collector
            System.gc();

            final Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            if(uri == null){
                Toast.makeText(this, getString(R.string.scan_error), Toast.LENGTH_SHORT).show();
                return;
            }
            if(request_scan_type == REQUEST_CIN){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(this, "Scan CIN ...", Toast.LENGTH_SHORT).show();
                        scanCINPost(uri);
                        Toast.makeText(MainActivity.this, "Scan CIN", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else if(request_scan_type == REQUEST_CHEQUE){
                //Toast.makeText(this, "Scan de cheque", Toast.LENGTH_SHORT).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanChequePost(uri);
                    }
                });
                Toast.makeText(MainActivity.this, "Scan Cheque", Toast.LENGTH_SHORT).show();
            }


        }
    }


    public void scanChequePost(final Uri uri){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(uri != null){
                    Intent intent = new Intent(MainActivity.this, ChequeActivity.class);
                    intent.putExtra("uri", uri.toString());
                    //finish();
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.scan_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void scanCINPost(final Uri uri){

       runOnUiThread(new Runnable() {
           @Override
           public void run() {
               if(uri != null){
                   Intent intent = new Intent(MainActivity.this, CINActivity.class);
                   intent.putExtra("uri", uri.toString());
                   //finish();
                   startActivity(intent);
               }
               else{
                   Toast.makeText(getApplicationContext(), R.string.scan_error, Toast.LENGTH_SHORT).show();
               }
           }
       });
    }




    /**
     * Configuring the carousels cards ( Adapter + animation )
     */
    private void configureCarouselCards() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        // set the page margin between pages to 10px
        viewPager.setPageMargin(50);

        // Create pager adapter to manage cards
        pagerAdapter = new CardFragmentPagerAdapter(getSupportFragmentManager(), dpToPixels(1, this));
        // Transformation that scales the selected card and adds a shadow
        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
        // activate scaling while swiping pages
        fragmentCardShadowTransformer.enableScaling(true);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
        // set the selected item
        viewPager.setCurrentItem(0);
        // Set the number of visible cards on the screen to 3
        viewPager.setOffscreenPageLimit(2);
    }

    /**
     * Change value in dp to pixels
     * @param dp
     * @param context
     * @return
     */
    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }


}
