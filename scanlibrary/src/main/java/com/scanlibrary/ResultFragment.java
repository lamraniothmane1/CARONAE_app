package com.scanlibrary;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Lamrani on 18/01/2018.
 */
public class ResultFragment extends Fragment {

    private View view;
    private ImageView scannedImageView;
    private Button doneButton, back_button;
    private ImageButton rotate_button_90, rotate_button_min_90;
    private Bitmap original;
    private ImageView iv_logo_header;

    private static ProgressDialogFragment progressDialogFragment;

    public ResultFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.result_layout, null);
        init();
        return view;
    }

    private void init() {
        // Imageview that contains the result of cropping
        scannedImageView = (ImageView) view.findViewById(R.id.scannedImage);
        // get the bitmap from the previous fragment (Scan fragment)
        Bitmap bitmap = getBitmap();
        // Display this bitmap on the imageView
        setScannedImage(bitmap);
        // button finish
        doneButton = (Button) view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new DoneButtonClickListener());
        // back button
        back_button = (Button) view.findViewById(R.id.btn_back);
        back_button.setOnClickListener(new BackClickedListener());
        // buttons for rotating the image result
        rotate_button_90 = (ImageButton) view.findViewById(R.id.btn_rotate_90);
        rotate_button_90.setOnClickListener(new RotateClickListener(90));
        rotate_button_min_90 = (ImageButton) view.findViewById(R.id.btn_rotate_min_90);
        rotate_button_min_90.setOnClickListener(new RotateClickListener(-90));

        iv_logo_header = (ImageView) view.findViewById(R.id.iv_logo_header);

        // change the logo header if it is available (by default it will show CARONAE's logo
        if(ScanActivity.bm_image_logo_header != null){
            iv_logo_header.setImageBitmap(ScanActivity.bm_image_logo_header);
        }

        // change scan button backcround color if it is available
        if(ScanActivity.btn_color != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //scanButton.setBackgroundTintList(ContextCompat.getColorStateList(getActivity(), Color.parseColor("#3F51B5")));
                doneButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(ScanActivity.btn_color)));
                back_button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(ScanActivity.btn_color)));
            }
        }

        // change scan button text if it is available
        if(ScanActivity.st_scan_button != null){
            doneButton.setText(ScanActivity.st_scan_button);
        }

        // change retry button text if it is available
        if(ScanActivity.st_retry_button != null){
            back_button.setText(ScanActivity.st_retry_button);
        }

        // change rotate left button
        if(ScanActivity.bm_rotate_left != null){
            rotate_button_min_90.setImageBitmap(ScanActivity.bm_rotate_left);
        }

        // change rotate right button
        if(ScanActivity.bm_rotate_right != null){
            rotate_button_90.setImageBitmap(ScanActivity.bm_rotate_right);
        }

    }

    private Bitmap getBitmap() {
        // get the uri result from previous fragment (Fragment scan)
        Uri uri = getUri();
        try {
            // get Bitmap from the retrieved Uri
            original = Utils.getBitmap(getActivity(), uri);
            //getActivity().getContentResolver().delete(uri, null, null);
            // enhance the bitmap
            //original = ScanActivity.getMagicColorBitmap(original);
            return original;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @return the retrieved Urifrom (Scan fragment)
     */
    private Uri getUri() {
        Uri uri = getArguments().getParcelable(ScanConstants.SCANNED_RESULT);
        return uri;
    }

    /**
     * Update image view
     * @param scannedImage the result image
     */
    public void setScannedImage(Bitmap scannedImage) {

        scannedImageView.setImageBitmap(scale_image(scannedImage));
        //scannedImageView.setImageBitmap(scannedImage);
    }

    /**
     * When the user clicks on finish scan button we will send the Uri result to the main activity in order to handle the next action with API
     */
    private class DoneButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            showProgressDialog(getResources().getString(R.string.loading));
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent data = new Intent();

                        // get URI from the bitmap
                        //Bitmap resized_bitmap = scale_image(original);
                        final Uri uri = Utils.getUri(getActivity(), original);
                        data.putExtra(ScanConstants.SCANNED_RESULT, uri);
                        getActivity().setResult(Activity.RESULT_OK, data);
                        original.recycle();
                        System.gc();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().finish();
                                dismissDialog();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissDialog();
                        Toast.makeText(getActivity(), getString(R.string.scan_error), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            });
        }
    }


    // for Magic color ( in order to enhance the quality of the image)
    // original = ScanActivity.getMagicColorBitmap(original);

    /**
     * Show a progress dialog
     * @param message
     */
    protected synchronized void showProgressDialog(String message) {
        if (progressDialogFragment != null && progressDialogFragment.isVisible()) {
            // Before creating another loading dialog, close all opened loading dialogs (if any)
            progressDialogFragment.dismissAllowingStateLoss();
        }
        progressDialogFragment = null;
        progressDialogFragment = new ProgressDialogFragment(message);
        FragmentManager fm = getFragmentManager();
        progressDialogFragment.show(fm, ProgressDialogFragment.class.toString());
    }

    /**
     * Dissmiss the progress dialog
     */
    protected synchronized void dismissDialog() {
        progressDialogFragment.dismissAllowingStateLoss();
    }

    /**
     * Rotation of the bitmap
     */
    private class RotateClickListener implements View.OnClickListener {

        int angle;

        public RotateClickListener(int angle){
            this.angle = angle;
        }

        @Override
        public void onClick(View v) {
            showProgressDialog("Rotation en cours");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final Bitmap rotated_bitmap = rotateBitmap(original, angle);
                    if(rotated_bitmap != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                original = rotated_bitmap;
                                scannedImageView.setImageBitmap(original);
                            }
                        });
                    }
                    dismissDialog();
                }
            });

        }
    }

    public  Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private class BackClickedListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getActivity().onBackPressed();
        }
    }


    public  Bitmap scale_image(Bitmap bitmap) {

        //int nh = (int) ( bitmap.getHeight() * (600.0 / bitmap.getWidth()) );

        int width = (int) (bitmap.getWidth() * 0.8);
        int height = (int) (bitmap.getHeight()*0.8);

        return  Bitmap.createScaledBitmap(bitmap, width, height, true);

    }


}