package com.scanlibrary;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scanlibrary.auto_detection.DocumentMessage;
import com.scanlibrary.auto_detection.HUDCanvasView;
import com.scanlibrary.auto_detection.ImageProcessor;
import com.scanlibrary.auto_detection.PreviewFrame;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Lamrani on 18/01/2018.
 */
public class ScanActivity extends AppCompatActivity implements SurfaceHolder.Callback, IScanner, Camera.PreviewCallback {

    //global variables
    private SurfaceView cameraView;
    private SurfaceHolder holder;
    public static Camera camera;

    public static ImageView btn_load_image, btn_capture_image, iv_logo_header;
    private Camera.Parameters parameters;
    private static int RESULT_LOAD_IMG = 1;
    private int REQUEST_CODE = 99;
    public static Bitmap bm_img_btn_capture;
    private boolean flash_enabled = false;
    public static boolean mode_auto_enabled = true;
    private Button btn_flash, btn_mode_auto;
    private static Camera.PictureCallback mPicture;

    public static Bitmap bm_btn_capture_camera, bm_btn_load_file, bm_image_logo_header, bm_back_button, bm_rotate_left, bm_rotate_right, bm_flash_button;
    public static String btn_color, st_scan_button, st_finish_button, st_retry_button, st_back_button, st_polygon_color;

    private static boolean is_camera_busy = false;

    private boolean imageProcessorBusy = false;
    private HUDCanvasView mHud;
    private HandlerThread mImageThread;
    private ImageProcessor mImageProcessor;
    public static Integer findDocument_counter = 0;
    public static boolean process_enabled = true;

    private android.hardware.Camera.Size pictureSize;

    private ScanFragment scanFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.scan_layout);

        setContentView(R.layout.activity_camera);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();

        // btn load image
        btn_load_image.setOnClickListener(new LoadFileClickListener());

        // btn capture image
        btn_capture_image.setOnClickListener(new CaptureImageClickListener());

        // action flash
        btn_flash.setOnClickListener(new FlashClickListener());

        // action mode automatic
        btn_mode_auto.setOnClickListener(new CaptureModeClickListener());

        cameraView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                //Toast.makeText(ScanActivity.this, "Touched screen", Toast.LENGTH_SHORT).show();
                if(camera != null && !is_camera_busy){

                    is_camera_busy = true;

                    camera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera) {
                            if(success){
                                //Toast.makeText(ScanActivity.this, "Autofocusing", Toast.LENGTH_SHORT).show();
                                Log.i("Autofocus", "Autofocusing");
                                //camera.takePicture(null, null, mPicture);
                            }
                            else{
                                //Toast.makeText(ScanActivity.this, "Can't autofocus", Toast.LENGTH_SHORT).show();
                                Log.i("Autofocus", "Can't autofocus");
                                //camera.takePicture(null, null, mPicture);
                            }

                            is_camera_busy = false;

                        }
                    });


                }

                return false;
            }
        });

       /* cameraView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();

                    Rect touchRect = new Rect(
                            (int) (x - 100),
                            (int) (y - 100),
                            (int) (x + 100),
                            (int) (y + 100));


                    final Rect targetFocusRect = new Rect(
                            touchRect.left * 2000 / cameraView.getWidth() - 1000,
                            touchRect.top * 2000 / cameraView.getHeight() - 1000,
                            touchRect.right * 2000 / cameraView.getWidth() - 1000,
                            touchRect.bottom * 2000 / cameraView.getHeight() - 1000);

                    doTouchFocus(targetFocusRect);
                }

                Toast.makeText(ScanActivity.this, "Touched", Toast.LENGTH_SHORT).show();

                return false;
            }
        });*/

    }


    private void init() {
        cameraView = (SurfaceView)findViewById(R.id.camera_view);

        btn_flash = (Button) findViewById(R.id.btn_flash);
        btn_mode_auto = (Button) findViewById(R.id.btn_mode_auto);

        btn_load_image = (ImageView) findViewById(R.id.btn_load_file);
        btn_capture_image = (ImageView) findViewById(R.id.btn_camera_capture);
        iv_logo_header = (ImageView) findViewById(R.id.iv_logo_header) ;

        bm_btn_capture_camera = getBtnCaptureBitmap();
        bm_btn_load_file = getBtnLoadFileBitmap();
        bm_image_logo_header = getLogoHeaderBitmap();
        st_scan_button = get_scan_button_string();
        st_finish_button = get_finish_button_string();
        btn_color = getButtonColor();
        st_retry_button = get_retry_button_string();
        st_back_button = get_back_button_string();
        st_polygon_color = get_polygon_color_string();
        bm_back_button = getBtnBackBitmap();
        bm_flash_button = getBtnFlashBitmap();
        bm_rotate_left = getBtnRotaeLeftBitmap();
        bm_rotate_right = getBtnRotaeRightBitmap();

        // image processor for document preview detect
        mHud = (HUDCanvasView) findViewById(R.id.hud);
        mImageThread = new HandlerThread("Worker Thread");
        mImageThread.start();
        mImageProcessor = new ImageProcessor(mImageThread.getLooper(), new Handler(), this);

        // change btn capture picture
        if(bm_btn_capture_camera != null){
            btn_capture_image.setImageBitmap(bm_btn_capture_camera);
        }
        // change btn load file from storage
        if(bm_btn_load_file != null){
            btn_load_image.setImageBitmap(bm_btn_load_file);
        }
        // change logo header
        if(bm_image_logo_header != null){
            iv_logo_header.setImageBitmap(bm_image_logo_header);
        }
        // change flash button image
        if(bm_flash_button != null){
            btn_flash.setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(bm_flash_button), null, null, null); ;
        }

        // request for camera permission and storage permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        // initialize the camera
        init_cameraView();

        // Auto focus configuration
        mPicture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                File pictureFile = null;
                try {
                    pictureFile = getOutputMediaFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (pictureFile == null) {
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    fos.write(data);
                    fos.close();

                    Uri uri = Uri.fromFile(pictureFile);

                    onBitmapSelect(uri);

                    // disable flash if enabled
                    if(flash_enabled){
                        turnOffFlash();
                    }

                } catch (FileNotFoundException e) {

                } catch (IOException e) {

                }
            }
        };

    }


    // get capture image button Bitmap
    protected Bitmap getBtnCaptureBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_BTN_CAPTURE);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Logo header Bitmap
    protected Bitmap getLogoHeaderBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_LOGO_HEADER);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Button load file bitmap
    protected Bitmap getBtnLoadFileBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_LOAD_FILE);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Color button
    protected String getButtonColor(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.BUTTON_COLOR);
        }
        return null;
    }

    // get Button flash bitmap
    protected Bitmap getBtnFlashBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_BTN_FLASH);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Button back bitmap
    protected Bitmap getBtnBackBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_BACK_BUTTON);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Button Rotate Right bitmap
    protected Bitmap getBtnRotaeRightBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_ROTATE_RIGHT);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get Button Rotate left bitmap
    protected Bitmap getBtnRotaeLeftBitmap(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            byte[] b = extras.getByteArray(Utils.IMG_ROTATE_LEFT);
            if(b != null){
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
        }
        return null;
    }

    // get scan button text
    protected String get_scan_button_string(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.STRING_SCAN);
        }
        return null;
    }

    // get finish button text
    protected String get_finish_button_string(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.STRING_FINISH);
        }
        return null;
    }

    // get retry button text
    protected String get_retry_button_string(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.STRING_RETRY);
        }
        return null;
    }

    // get back button text
    protected String get_back_button_string(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.STRING_BACK_BUTTON);
        }
        return null;
    }

    // get back button text
    protected String get_polygon_color_string(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            return extras.getString(Utils.STRING_CONTOUR_COLOR);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.content);
        super.onBackPressed();
        if(fragment instanceof ScanFragment){
            refreshCamera();
            is_camera_busy = false;
            process_enabled = true;
            getHUD().clear();
            getHUD().invalidate();
        }
    }

    /**
     * When a bit map is selected either from the camera or from the gallery, we pass the uri to ScanFragment in order to make the right processing
     * @param uri : the original image Uri
     */
    @Override
    public void onBitmapSelect(Uri uri) {
        ScanFragment fragment = new ScanFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SELECTED_BITMAP, uri);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.addToBackStack(ScanFragment.class.toString());
        fragmentTransaction.commit();
    }

    /**
     * When the scan is finished, we pass the result Uri to the Result fragment.
     * @param uri : the scanned image Uri
     */
    @Override
    public void onScanFinish(Uri uri) {
        ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SCANNED_RESULT, uri);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.addToBackStack(ResultFragment.class.toString());
        fragmentTransaction.commit();
    }

    @Override
    public void onTrimMemory(int level) {
        switch (level) {
            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                /*
                   Release any UI objects that currently hold memory.

                   The user interface has moved to the background.
                */
                break;
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                /*
                   Release any memory that your app doesn't need to run.

                   The device is running low on memory while the app is running.
                   The event raised indicates the severity of the memory-related event.
                   If the event is TRIM_MEMORY_RUNNING_CRITICAL, then the system will
                   begin killing background processes.
                */
                break;
            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                /*
                   Release as much memory as the process can.

                   The app is on the LRU list and the system is running low on memory.
                   The event raised indicates where the app sits within the LRU list.
                   If the event is TRIM_MEMORY_COMPLETE, the process will be one of
                   the first to be terminated.
                */
                new AlertDialog.Builder(this)
                        .setTitle(R.string.low_memory)
                        .setMessage(R.string.low_memory_message)
                        .create()
                        .show();
                break;
            default:
                /*
                  Release any non-critical data structures.

                  The app received an unrecognized memory level value
                  from the system. Treat this as a generic low-memory message.
                */
                break;
        }
    }


    private File getOutputMediaFile() throws IOException {

        // Create a media file name
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyCameraApp");
        //File mediaStorageDir =  Environment.getExternalStorageDirectory();
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        //Toast.makeText(this, "File saved", Toast.LENGTH_SHORT).show();

        return mediaFile;
    }



    private File getDir() {
        File sdDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "CameraAPIDemo");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
            surfaceCreated(holder);
        }
        else{
            disableButtons();
            Toast.makeText(this, "Permissions denied", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Disable buttons
     */
    private void disableButtons() {
        btn_load_image.setEnabled(false);
        btn_capture_image.setEnabled(false);
    }

    /**
     * Activate buttons
     */
    private void activateButtons() {
        btn_load_image.setEnabled(true);
        btn_capture_image.setEnabled(true);
    }


    /**
     * Initialize holders for camera view and draw help rectangle
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void init_cameraView() {
        holder = cameraView.getHolder();

        holder.addCallback((SurfaceHolder.Callback) this);

        //holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        cameraView.setSecure(true);

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        cameraPreviewConfiguration();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void cameraPreviewConfiguration() {
        try {
            //open a camera
            camera = Camera.open();

            pictureSize = camera.getParameters().getPreviewSize();

            if(camera != null){
                camera.setPreviewCallback(this);
            }

        }
        catch (Exception e) {
            Log.i("Exception", e.toString());
            return;
        }

        /* Set Auto focus */
        parameters = camera.getParameters();
        List<String> focusModes = parameters.getSupportedFocusModes();
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)){
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }


        // new

        Camera.Size pSize = getMaxPreviewResolution();

        pictureSize = pSize;

        parameters.setPreviewSize(pSize.width, pSize.height);

        float previewRatio = (float) pSize.width / pSize.height;

        Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getRealSize(size);

        int displayWidth = Math.min(size.y, size.x);
        int displayHeight = Math.max(size.y, size.x);

        float displayRatio = (float) displayHeight / displayWidth;

        int previewHeight = displayHeight;

        if (displayRatio > previewRatio) {
            ViewGroup.LayoutParams surfaceParams = cameraView.getLayoutParams();
            previewHeight = (int) ((float) size.y / displayRatio * previewRatio);
            surfaceParams.height = previewHeight;
            cameraView.setLayoutParams(surfaceParams);

            //mHud.getLayoutParams().height = previewHeight;
        }

        Camera.Size maxRes = getMaxPictureResolution(previewRatio);
        if (maxRes != null) {
            parameters.setPictureSize(maxRes.width, maxRes.height);
            Log.d("Camera view", "max supported picture resolution: " + maxRes.width + "x" + maxRes.height);
        }

        // end new


        /* Set orientation of the camera */
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(0, info);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);

        // set exposure to the max
        //parameters.setExposureCompensation(parameters.getMaxExposureCompensation());
        //parameters.setAutoExposureLock(true);
        //parameters.setAutoWhiteBalanceLock(true);


        parameters.setRotation(result);




        camera.setParameters(parameters);


        // **************

        // *************

        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }
        catch (Exception e) {
            return;
        }

    }

    public List<Camera.Size> getPictureResolutionList() {
        return camera.getParameters().getSupportedPictureSizes();
    }

    public Camera.Size getMaxPictureResolution(float previewRatio) {
        int maxPixels = 0;
        int ratioMaxPixels = 0;
        Camera.Size currentMaxRes = null;
        Camera.Size ratioCurrentMaxRes = null;
        for (Camera.Size r : getPictureResolutionList()) {
            float pictureRatio = (float) r.width / r.height;
            Log.d("Camera view", "supported picture resolution: " + r.width + "x" + r.height + " ratio: " + pictureRatio);
            int resolutionPixels = r.width * r.height;

            if (resolutionPixels > ratioMaxPixels && pictureRatio == previewRatio) {
                ratioMaxPixels = resolutionPixels;
                ratioCurrentMaxRes = r;
            }

            if (resolutionPixels > maxPixels) {
                maxPixels = resolutionPixels;
                currentMaxRes = r;
            }
        }

        SharedPreferences mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean matchAspect = mSharedPref.getBoolean("match_aspect", true);

        if (ratioCurrentMaxRes != null && matchAspect) {

            Log.d("Caùera view", "Max supported picture resolution with preview aspect ratio: "
                    + ratioCurrentMaxRes.width + "x" + ratioCurrentMaxRes.height);
            return ratioCurrentMaxRes;
        }
        return currentMaxRes;
    }


    public Camera.Size getMaxPreviewResolution() {
        int maxWidth = 0;
        Camera.Size curRes = null;

        camera.lock();

        for (Camera.Size r : getResolutionList()) {
            if (r.width > maxWidth) {
                Log.d("Camera View", "supported preview resolution: " + r.width + "x" + r.height);
                maxWidth = r.width;
                curRes = r;
            }
        }
        return curRes;
    }

    public List<Camera.Size> getResolutionList() {
        return camera.getParameters().getSupportedPreviewSizes();
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera(); //call method for refress camera
    }

    public void refreshCamera() {
        if (holder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
            camera.setPreviewDisplay(holder);

            is_camera_busy = false;
            process_enabled = true;

            camera.startPreview();

            if(camera != null){
                camera.setPreviewCallback(this);
            }
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //for release a camera
        if(camera != null){
            camera.stopPreview();
            //camera.release();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                //scannedImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                onBitmapSelect(selectedImage);

            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }



    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            //Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    /**
     * Load a file from the Gallery
     */
    private class LoadFileClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            // Create intent to Open Image applications like Gallery, Google Photos
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }

    /**
     * Capture an Image from the Camera
     */
    private class CaptureImageClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            take_picture();
        }
    }

    /**
     * Auto focusing the camera
     */
    public static void take_picture(){
        if(!is_camera_busy){
            is_camera_busy = true;
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                   try {
                       process_enabled = false;
                       if(success){
                           Log.i("AutoFocusing", "true");
                           camera.takePicture(null, null, mPicture);
                       }
                       else{
                           Log.i("AutoFocusing", "false");
                           camera.takePicture(null, null, mPicture);
                       }
                   }
                   catch (Exception e){
                       e.printStackTrace();
                   }
                    is_camera_busy = false;
                }
            });
        }
    }

    private class FlashClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            btn_flash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(flash_enabled){
                        turnOffFlash();
                    }
                    else{
                        turnOnFlash();
                    }
                }
            });
        }
    }

    private class CaptureModeClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if(mode_auto_enabled){
                turnOffAutoMode();
            }
            else{
                turnOnAutoMode();
            }
        }
    }

    /**
     * Turning on the automatic mode to detect documents
     */
    private void turnOnAutoMode() {
        mode_auto_enabled = true;
        btn_mode_auto.setText(R.string.mode_auto_enabled);
    }

    /**
     * Turning off the automatic mode to detect documents
     */
    private void turnOffAutoMode() {
        mode_auto_enabled = false;
        btn_mode_auto.setText(R.string.mode_auto_disabled);
    }


    /**
     * Turning on the flash of camera
     */
    private void turnOnFlash() {
        if (!flash_enabled) {
            if (camera == null ||  parameters == null) {
                return;
            }
            parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
            flash_enabled = true;
            btn_flash.setText(R.string.flash_enabled);
        }
    }

    /**
     * Turning off the flash of camera
     */
    private void turnOffFlash() {
        if (flash_enabled) {
            if (camera == null || parameters == null) {
                return;
            }
            parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            flash_enabled = false;
            btn_flash.setText(R.string.flash_disabled);
        }
    }

    // Document autodetection functions

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        if(pictureSize != null){
            Log.d("Camera preview", "onPreviewFrame - received image " + pictureSize.width + "x" + pictureSize.height
                    + " focused: " + "?" + " imageprocessor: " + (imageProcessorBusy ? "busy" : "available"));

            Log.d("ImageProcessor busy", String.valueOf(imageProcessorBusy));

            if(!imageProcessorBusy && process_enabled){
                setImageProcessorBusy(true);

                Mat yuv = new Mat(new Size(pictureSize.width, pictureSize.height * 1.5), CvType.CV_8UC1);
                yuv.put(0, 0, bytes);

                Mat mat = new Mat(new Size(pictureSize.width, pictureSize.height), CvType.CV_8UC4);
                Imgproc.cvtColor(yuv, mat, Imgproc.COLOR_YUV2RGBA_NV21, 4);

                yuv.release();

                sendImageProcessorMessage("previewFrame", new PreviewFrame(mat, true, false));
            }

           /* if(!imageProcessorBusy && process_enabled){
                setImageProcessorBusy(true);

                Bitmap bitmap = getBitmap(bytes);

                scanFragment = new ScanFragment();

                scanFragment.setOriginal(bitmap);
                scanFragment.setPolygonView(new PolygonView(getApplicationContext()));

                Map<Integer, PointF> pointFs = scanFragment.getEdgePoints(bitmap);

                drawDocumentBox(pointFs, bitmap.getWidth(), bitmap.getHeight());

                setImageProcessorBusy(false);

                getHUD().clear();
                getHUD().invalidate();
            }*/
        }
    }

    private Bitmap getBitmap(byte[] data){

        try {

            Camera.Parameters parameters = camera.getParameters();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            YuvImage yuvImage = new YuvImage(data, parameters.getPreviewFormat(), parameters.getPreviewSize().width, parameters.getPreviewSize().height, null);
            yuvImage.compressToJpeg(new Rect(0, 0, parameters.getPreviewSize().width, parameters.getPreviewSize().height), 90, out);
            byte[] imageBytes = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

            out.flush();
            out.close();

            return bitmap;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return  null;
    }

    private void drawDocumentBox(Map<Integer, PointF> pointFs, float previewWidth, float previewHeight) {

        Path path = new Path();

        HUDCanvasView hud = getHUD();

        // ATTENTION: axis are swapped

        float previewWidth_per = previewHeight;
        float previewHeight_per = previewWidth;

        path.moveTo( previewWidth - (float) pointFs.get(0).y, (float) pointFs.get(0).x );
        path.lineTo( previewWidth - (float) pointFs.get(1).y, (float) pointFs.get(1).x );
        path.lineTo( previewWidth - (float) pointFs.get(2).y, (float) pointFs.get(2).x );
        path.lineTo( previewWidth - (float)pointFs.get(3).y, (float) pointFs.get(3).x );
        path.close();

        PathShape newBox = new PathShape(path , previewWidth , previewHeight);

        Paint paint = new Paint();
        paint.setColor(Color.argb(64, 0, 255, 0));

        Paint border = new Paint();
        border.setColor(Color.rgb(0, 255, 0));
        border.setStrokeWidth(5);

        hud.clear();
        hud.addShape(newBox, paint, border);
        invalidateHUD();
    }

    public void sendImageProcessorMessage(String messageText, Object obj) {
        Log.d("Image Processing", "sending message to ImageProcessor: " + messageText + " - " + obj.toString());
        Message msg = mImageProcessor.obtainMessage();
        msg.obj = new DocumentMessage(messageText, obj);
        mImageProcessor.sendMessage(msg);
    }

    public void setImageProcessorBusy(boolean imageProcessorBusy) {
        this.imageProcessorBusy = imageProcessorBusy;
    }


    public HUDCanvasView getHUD() {
        return mHud;
    }

    public void invalidateHUD() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mHud.invalidate();
            }
        });
    }

    // Native functions

    public static native Bitmap getScannedBitmap(Bitmap bitmap, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

    public static native Bitmap getGrayBitmap(Bitmap bitmap);

    public static native Bitmap getMagicColorBitmap(Bitmap bitmap);

    public static native Bitmap getBWBitmap(Bitmap bitmap);

    public static native float[] getPoints(Bitmap bitmap);

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("Scanner");
    }
}